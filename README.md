PHP environment
===============
PHP + NGINX + MYSQL + REDIS

Installs latest PHP with most usefull extensions


Installation
------------
Install docker (https://www.docker.com/)

Enable drive D: in docker settings: Settings->Shared Drives check checkbox near the D drive
 
Screen shot: http://share.smartinfosys.com/webteam/as/2016-10-12_17-23-43.png


    cd d:/

    git checkout https://AlexeyS@bitbucket.org/AlexeyS/docker.git


Install and build containers:

    docker-compose up -d --build



Next time just run without build in background mode:

    docker-compose up -d
